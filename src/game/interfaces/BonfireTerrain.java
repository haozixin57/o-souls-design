package game.interfaces;

import edu.monash.fit2099.engine.Location;

public interface BonfireTerrain {
    void lightTheBonfire();
    Location getLocation();
}
